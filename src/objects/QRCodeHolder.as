package objects
{
	import flash.display.Bitmap;
	import flash.display.StageDisplayState;
	
	import data.DataHandler;
	
	import feathers.controls.Button;
	import feathers.controls.ButtonGroup;
	import feathers.controls.ImageLoader;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.core.FeathersControl;
	import feathers.data.ArrayCollection;
	import feathers.layout.Direction;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalAlign;
	import feathers.layout.VerticalLayout;
	
	import org.qrcode.QRCode;
	
	import starling.core.Starling;
	import starling.events.Event;
	import starling.textures.Texture;
	
	public class QRCodeHolder extends FeathersControl
	{
		private var qr:QRCode;
		private var walletNameLabel:Label;
		private var qrCodeImage:ImageLoader;
		private var layoutVerMedCenter:VerticalLayout;
		private var mainCont:LayoutGroup;
		private var subCont:LayoutGroup;
		private var changeWalletBtn:Button;
		private var buttonsGroup:ButtonGroup
		private var layoutHorMedCenter:HorizontalLayout;
		private var paidBtn:Button;
		
		public function QRCodeHolder()
		{
			super();

			
		}
		override protected function initialize():void
		{

			layoutVerMedCenter = new VerticalLayout;
			layoutVerMedCenter.verticalAlign =  VerticalAlign.MIDDLE;
			layoutVerMedCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutHorMedCenter = new HorizontalLayout;
			layoutHorMedCenter.verticalAlign =  VerticalAlign.MIDDLE;
			layoutHorMedCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			mainCont = new LayoutGroup;
			mainCont.layout = layoutVerMedCenter;
	
			subCont = new LayoutGroup;
			subCont.layout = layoutHorMedCenter;
			
			walletNameLabel = new Label;
//			walletNameLabel.paddingTop = 15;
			changeWalletBtn = new Button;
			changeWalletBtn.label = "Change Wallet";
			changeWalletBtn.addEventListener(Event.TRIGGERED, onChangeBtnTriggered);
			
			subCont.addChild(walletNameLabel);
//			subCont.addChild(changeWalletBtn);
			
				
			qr = new QRCode;
			qrCodeImage = new ImageLoader;
			
			if(DataHandler.selectedWalletName !="" && DataHandler.selectedWalletName!=null)
			{
				trace("DataHandler.selectedWalletName="+DataHandler.selectedWalletName+".");
				CreatQRCodeImage(DataHandler.selectedWalletAddress);
//				walletNameLabel.text = "Selected Wallet: "+DataHandler.selectedWalletName;
			}
			
			buttonsGroup = new ButtonGroup;
			buttonsGroup.direction = Direction.HORIZONTAL;
			buttonsGroup.dataProvider = new ArrayCollection(
				[
					{ label: "Send email", triggered: emailBtn_triggeredHandler}
					,{ label: "Scan QR", triggered: qrBtn_triggeredHandler }
//					,{ label: "Paid", triggered: paidBtn_triggeredHandler }
				]);
			
			paidBtn = new Button;
			paidBtn.label = "paid";
			paidBtn.addEventListener(Event.TRIGGERED, paidBtn_triggeredHandler);
			
			mainCont.addChild(subCont);
			mainCont.addChild(qrCodeImage);
			mainCont.addChild(buttonsGroup);
			mainCont.addChild(paidBtn);
			this.addChild(mainCont);
			
		}
		
		private function onChangeBtnTriggered(e:Event):void
		{
			DataHandler.mainView.ViewSettingsPanel();
		}
		
		private function emailBtn_triggeredHandler(e:Event):void
		{
			if(!DataHandler.isCurrentDataSaved)
			{
				DataHandler.mainView.customerPanel.SaveCustomerData();
			}
			DataHandler.SendEmail();
			
		}
		private function qrBtn_triggeredHandler(e:Event):void
		{
			DataHandler.mainView.ViewScanScreen();
			if(!DataHandler.isCurrentDataSaved)
			{
				DataHandler.mainView.customerPanel.SaveCustomerData();
				DataHandler.mainView.customerPanel.invoicePanel.SaveInvoiceData();
				DataHandler.isCurrentDataSaved = true;
			}
		}
		private function paidBtn_triggeredHandler(e:Event):void
		{
			DataHandler.Paid();
		}
		
		override protected function draw():void
		{	
			mainCont.setSize(actualWidth,actualHeight);	
			subCont.setSize(actualWidth * 0.85, 50);
			walletNameLabel.setSize(actualWidth * 0.6,40);
			changeWalletBtn.setSize(actualWidth * 0.25,40);
			qrCodeImage.setSize(actualWidth * 0.85, actualWidth * 0.85);
			buttonsGroup.setSize(actualWidth * 0.85, 100);
			paidBtn.setSize(actualWidth * 0.4, 40);
			buttonsGroup.gap = 10;
			buttonsGroup.paddingTop = 30;
			buttonsGroup.paddingBottom = 30;
		}
		
		public function CreatQRCodeImage(str:String):void
		{
			var bitmap:Bitmap;
			qr.encode(str);
			bitmap = new Bitmap(qr.bitmapData);
			bitmap.smoothing = true;
			var texture:Texture = Texture.fromBitmap(bitmap);
			qrCodeImage.source = texture;
			walletNameLabel.text = "Selected Wallet:\n"+DataHandler.selectedWalletName;
		}
	}
}