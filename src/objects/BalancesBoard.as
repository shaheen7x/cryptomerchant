package objects
{
	import flash.text.Font;
	import flash.text.FontType;
	
	import mx.core.FontAsset;
	
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.core.FeathersControl;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalAlign;
	
	import starling.text.TextFormat;
	
	public class BalancesBoard extends FeathersControl
	{
		private var mainCont:LayoutGroup;
		private var layoutHorMedCenter:HorizontalLayout;
		private var _balanceBTC:String;
		private var _balanceETH:String;
		private var _balanceBCH:String;
		private var _balanceLTC:String;
		private var labelBTC:Label;
		private var labelETH:Label;
		private var labelBCH:Label;
		private var labelLTC:Label;
		private var balanceLabelBTC:Label;
		private var balanceLabelETH:Label;
		private var balanceLabelBCH:Label;
		private var balanceLabelLTC:Label;
		private var notConnectedLabel:Label;
		private var refreshLabel:Label;
		
		public function BalancesBoard(BTC:String = "0.00000000",ETH:String = "0.00000000",BCH:String = "0.00000000",LTC:String = "0.00000000")
		{
			_balanceBTC = BTC;
			_balanceETH = ETH;
			_balanceBCH = BCH;
			_balanceLTC = LTC;
			
			super();
		}
		override protected function initialize():void
		{

			layoutHorMedCenter = new HorizontalLayout;
			layoutHorMedCenter.verticalAlign =  VerticalAlign.MIDDLE;
			layoutHorMedCenter.horizontalAlign = HorizontalAlign.CENTER;			
			
			mainCont = new LayoutGroup;
			mainCont.layout = layoutHorMedCenter;
			
			labelBTC = new Label;
			labelBTC.text = "BTC Balance:";
			labelBTC.fontStyles = new TextFormat("Verdana",14,0xE6903A);
			
			labelETH = new Label;
			labelETH.text = "ETH Balance:";
			labelETH.fontStyles = new TextFormat("Verdana",14,0xE6903A);
			
			labelBCH = new Label;
			labelBCH.text = "BCH Balance:";
			labelBCH.fontStyles = new TextFormat("Verdana",14,0xE6903A);
			
			labelLTC = new Label;
			labelLTC.text = "LTC Balance:";
			labelLTC.fontStyles = new TextFormat("Verdana",14,0xE6903A);
			
			balanceLabelBTC = new Label;
			balanceLabelBTC.text = _balanceBTC;
			balanceLabelBTC.fontStyles = new TextFormat("Verdana",13,0xE6E6E6);
			
			balanceLabelETH = new Label;
			balanceLabelETH.text = _balanceETH;
			balanceLabelETH.fontStyles = new TextFormat("Verdana",13,0xE6E6E6);
			
			balanceLabelBCH = new Label;
			balanceLabelBCH.text = _balanceBCH;
			balanceLabelBCH.fontStyles = new TextFormat("Verdana",13,0xE6E6E6);
			
			balanceLabelLTC = new Label;
			balanceLabelLTC.text = _balanceLTC;
			balanceLabelLTC.fontStyles = new TextFormat("Verdana",13,0xE6E6E6);
			
			notConnectedLabel = new Label;
			notConnectedLabel.text = "Please connect to your Coinbase to get your balance";
			notConnectedLabel.fontStyles = new TextFormat("Verdana",14,0xE6E6E6);
			
			refreshLabel = new Label;
			refreshLabel.text = "Please wait while reconnecting to your Coinbase";
			refreshLabel.fontStyles = new TextFormat("Verdana",14,0xE6E6E6);
			
			
			mainCont.addChild(labelBTC);
			mainCont.addChild(balanceLabelBTC);
			mainCont.addChild(labelETH);
			mainCont.addChild(balanceLabelETH);
			mainCont.addChild(labelBCH);
			mainCont.addChild(balanceLabelBCH);
			mainCont.addChild(labelLTC);
			mainCont.addChild(balanceLabelLTC);			
			
			this.addChild(mainCont);
			this.addChild(notConnectedLabel);
			this.addChild(refreshLabel);
			DisconnectBalance();
		}
		public function UpdateBalances(BTC:String = "0.00000000",ETH:String = "0.00000000",BCH:String = "0.00000000",LTC:String = "0.00000000"):void
		{
			balanceLabelBTC.text = BTC;
			balanceLabelETH.text = ETH;
			balanceLabelBCH.text = BCH;
			balanceLabelLTC.text = LTC;
			
			notConnectedLabel.visible = false;
			refreshLabel.visible = false;
			mainCont.visible = true;
		}
		public function DisconnectBalance():void
		{
			mainCont.visible = false;
			refreshLabel.visible = false;
			notConnectedLabel.visible = true;
		}
		public function RefreshBalance():void
		{
			mainCont.visible = false;
			refreshLabel.visible = true;
			notConnectedLabel.visible = false;
		}
		override protected function draw():void
		{

			mainCont.setSize(actualWidth,actualHeight);
			labelBTC.setSize(actualWidth * 0.13 ,actualHeight);
			balanceLabelBTC.setSize(actualWidth * 0.12 ,actualHeight);
			labelETH.setSize(actualWidth * 0.13 ,actualHeight);
			balanceLabelETH.setSize(actualWidth * 0.12 ,actualHeight);
			labelBCH.setSize(actualWidth * 0.13 ,actualHeight);
			balanceLabelBCH.setSize(actualWidth * 0.12 ,actualHeight);
			labelLTC.setSize(actualWidth * 0.13 ,actualHeight);
			balanceLabelLTC.setSize(actualWidth * 0.12 ,actualHeight);
			notConnectedLabel.setSize(actualWidth ,actualHeight);
			refreshLabel.setSize(actualWidth ,actualHeight);
		}
	}
}