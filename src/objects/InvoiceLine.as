package objects
{
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.core.FeathersControl;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalAlign;
	
	public class InvoiceLine extends FeathersControl
	{
		private var layoutHorMedCenter:HorizontalLayout;
		private var mainCont:LayoutGroup;
		private var _item:String;
		private var _quantity:String;
		private var _price:String;
		private var _total:Number;
		private var itemLabel:Label;
		private var quantiyLabel:Label;
		private var priceLabel:Label;
		private var totalLabel:Label;
		private var _totalText:String;
		public function InvoiceLine(item:String, price:String, quantity:String,total:Number = 0,totalText:String=""):void
		{
			_item = item;
			_price = price;
			_quantity = quantity;
			_total = total;
			_totalText = totalText
			super();
		}
		override protected function initialize():void
		{

			
			layoutHorMedCenter = new HorizontalLayout;
			layoutHorMedCenter.verticalAlign =  VerticalAlign.MIDDLE;
			layoutHorMedCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			mainCont = new LayoutGroup;
			mainCont.layout = layoutHorMedCenter;	
			
			itemLabel = new Label;
			itemLabel.text = _item;
			quantiyLabel = new Label;
			quantiyLabel.text = _quantity;
			priceLabel = new Label;
			priceLabel.text = _price;
			totalLabel = new Label;
			
			totalLabel.text = (_totalText=="")?(Number(_quantity)*Number(_price)).toFixed(2):_totalText;
			
			mainCont.addChild(itemLabel);
			mainCont.addChild(priceLabel);
			mainCont.addChild(quantiyLabel);
			mainCont.addChild(totalLabel);
				
			this.addChild(mainCont);
		}
		
		override protected function draw():void
		{
			layoutHorMedCenter.gap = 6;
			mainCont.setSize(actualWidth*0.9,30);	
			itemLabel.setSize(actualWidth* 0.4,30);
			quantiyLabel.setSize(actualWidth* 0.18,30);
			priceLabel.setSize(actualWidth* 0.15,30);
			totalLabel.setSize(actualWidth* 0.15,30);
		}
		public function UpdateVlaues(item:String, price:String, quantity:String,total:Number = 0,totalText:String=""):void
		{
			this.itemLabel.text = item;
			this.quantiyLabel.text = quantity;
			this.priceLabel.text = price;			
			this.totalLabel.text = (totalText=="")?(Number(quantity)*Number(price)).toFixed(2):totalText;
			trace("totalLabel.text "+totalLabel.text +" "+totalText );
		}
	}
}