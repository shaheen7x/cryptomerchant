package core
{
	import assets.Assets;
	
	import screens.MainView;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	import themes.CustomTheme;
	
	public class MainApp extends Sprite
	{
		public static var theme:CustomTheme;
		private var mainView:MainView;
		
		public function MainApp()
		{
			super();
			this.addEventListener(starling.events.Event.ADDED_TO_STAGE, onAddedToStage);
			
		}
		private function onAddedToStage(e:starling.events.Event):void
		{
			this.removeEventListener(starling.events.Event.ADDED_TO_STAGE, onAddedToStage);
			trace("MainApp added to Stage");
			Assets.init(this);
		}
		
		
		public function StartApp():void
		{
			theme = new CustomTheme;
			
			mainView = new MainView;
			mainView.setSize(950,750);
			this.addChild(mainView);
		}
	}
}