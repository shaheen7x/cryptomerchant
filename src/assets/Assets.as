package assets
{
	import flash.filesystem.File;
	
	import core.MainApp;
	
	import starling.utils.AssetManager;

	public class Assets
	{
		public static var assetsManager:AssetManager = new AssetManager;
		private static var mainApp:MainApp;
		
		public static function init(_mainApp:MainApp):void
		{
			mainApp = _mainApp;
			var appDir:File = File.applicationDirectory;
//			trace(appDir.url);
//			trace(appDir.nativePath);
			assetsManager.enqueue(appDir.resolvePath("assets/graphics"));	
			assetsManager.loadQueue(QueueLoading);
		}
		private static function QueueLoading(ratio:Number):void
		{
//				trace("Loading assets, progress:", ratio);				
// when the ratio equals '1', we are finished.
			if (ratio == 1.0)
				mainApp.StartApp();
		}
	}
}