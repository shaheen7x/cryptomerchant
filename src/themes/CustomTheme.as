package themes
{
	import feathers.themes.MetalWorksDesktopTheme;
	import feathers.themes.MetalWorksMobileTheme;
	import feathers.themes.TopcoatLightMobileTheme;
	
	public class CustomTheme extends MetalWorksMobileTheme
	{
		private static const COLOR_BACKGROUND:uint = 0x333333;
		
		public function CustomTheme()
		{

			super();
			this.starling.stage.color = COLOR_BACKGROUND;
			this.starling.nativeStage.color = COLOR_BACKGROUND;
		}
	}
}