package data
{
	import com.adobe.protocols.oauth2.OAuth2;
	import com.adobe.protocols.oauth2.OAuth2Const;
	import com.adobe.protocols.oauth2.event.GetAccessTokenEvent;
	import com.adobe.protocols.oauth2.event.RefreshAccessTokenEvent;
	import com.adobe.protocols.oauth2.grant.AuthorizationCodeGrant;
	import com.adobe.protocols.oauth2.grant.IGrantType;
	
	import flash.events.Event;
	import flash.media.StageWebView;
	import flash.net.FileReference;
	import flash.net.SharedObject;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.utils.ByteArray;
	
	import org.as3commons.logging.setup.LogSetupLevel;
	
	import screens.MainView;
	import screens.WalletPanel;
	import screens.WebViewPanel;

	public class DataHandler
	{
		public static const MERCHANDISER_NAME:String = "Merchandiser Name";
		public static const SHARED_OBJECT:String = "CRYPTOBILL";
		private static const emailPhpURL:String = "https://shaheen7xgames.000webhostapp.com/SendMail/send_mail.php";
		public static var walletsArray:Array = new Array;
		public static var selectedWalletID:int;
		public static var selectedWalletName:String;
		public static var selectedWalletAddress:String;
		public static var marketerPass:String = "123456";
		private static var _mainView:MainView;
		public static var customersArray:Array = new Array;
		public static var customersDataArray:Array = new Array;
		public static var tempInvoiceArray:Array = new Array;
		public static var invoicesArray:Array = new Array;
		public static var selectedCustomerID:int;
		private static var _isCurrentDataSaved:Boolean = false;
		private static var _isCoinbaseConnected:Boolean = false;
		public static var coinbaseDataArray:Array = new Array;
		public static var coinbaseWalletsArray:Array = new Array;
		private static var coinbaseCodesIndexArray:Array = new Array;
		private static var coinbaseAccessToken:String;
		private static var coinbaseRefreshToken:String;
		public static var emailSenderURL:String;
		private static var _isGmailConnected:Boolean = false;
		private static var gmailAccessToken:String;
		private static var gmailRefreshToken:String;
		public static var fees:Number = 0;
		
		public static function StoreAppData():void
		{
			var sharedObject:SharedObject = SharedObject.getLocal(SHARED_OBJECT);
			sharedObject.data.selectedWalletID = selectedWalletID;
			sharedObject.data.selectedWalletName = selectedWalletName;
			sharedObject.data.selectedWalletAddress = selectedWalletAddress;
			sharedObject.data.walletsArray = walletsArray;
			sharedObject.data.customersArray = customersArray;
			sharedObject.data.customersDataArray = customersDataArray;
			sharedObject.data.invoicesArray = invoicesArray;
			sharedObject.data.emailSenderURL = emailSenderURL;
			sharedObject.data.fees = fees;
			sharedObject.data.marketerPass = marketerPass;
//			var tempCustomersArray:Array = new Array;
//			for(var i:int=0; i<customersDataArray.length; i++)
//			{
//				tempCustomersArray.push(JSON.stringify(customersDataArray[i]));
//			}
//			sharedObject.data.customersDataArray = tempCustomersArray;
			sharedObject.flush();
		}
		
		public static function getAppData():void
		{
			var sharedObject:SharedObject = SharedObject.getLocal(SHARED_OBJECT);
			selectedWalletID = sharedObject.data.selectedWalletID
			selectedWalletName = sharedObject.data.selectedWalletName;
			selectedWalletAddress = sharedObject.data.selectedWalletAddress;
			walletsArray = (sharedObject.data.walletsArray) ? sharedObject.data.walletsArray : new Array;
			customersArray = (sharedObject.data.customersArray) ? sharedObject.data.customersArray : new Array;
			customersDataArray = (sharedObject.data.customersDataArray) ? sharedObject.data.customersDataArray : new Array;
			invoicesArray = (sharedObject.data.invoicesArray) ? sharedObject.data.invoicesArray : new Array;
			emailSenderURL = sharedObject.data.emailSenderURL;
			fees = sharedObject.data.fees;
			if(!fees) fees = 0;
			marketerPass = sharedObject.data.marketerPass;
			if(!marketerPass) marketerPass = "123456";
			if(selectedWalletID > walletsArray.length-1)
			{
				selectedWalletID = -1;
			}
			trace("selectedWalletID "+selectedWalletID);
			for(var i:int=0; i<invoicesArray.length; i++)
			{
				trace(invoicesArray[i].name);
				trace(invoicesArray[i]);
				trace(JSON.stringify(invoicesArray[i]));
			}
		}
		
		public static function AddNewWallet(name:String, address:String):void
		{
			var obj:Object = {name:name,address:address, text:name+": "+address};
			walletsArray.insertAt(0,obj);
		}
		
		public static function CalculateInvoice(item:String="", price:String="", quantity:String=""):Number
		{
			if(item!="" && price!="" && quantity!="")
			{
				var amount:Number = Number(price) * Number(quantity);
				tempInvoiceArray.push({item:item, price:price, quantity:quantity, amount:amount});
			}
			var total:Number = 0;
			for(var i:int=0; i<tempInvoiceArray.length; i++)
			{
				total += tempInvoiceArray[i].amount;
			}
			return total;
		}
		
		public static function SelectWallet(selectedIndex:int):void
		{
			var wallets:Array = Wallets();
			selectedWalletID = selectedIndex;
			selectedWalletName = wallets[selectedIndex].name;
			selectedWalletAddress = wallets[selectedIndex].address;
			mainView.qrHolder.CreatQRCodeImage(selectedWalletAddress);
		}

		public static function get mainView():MainView
		{
			return _mainView;
		}

		public static function set mainView(value:MainView):void
		{
			_mainView = value;
		}

		public static function AddNewCustomer(name:String, address:String, phone:String, email:String):void
		{
			var id:int = customersArray.length;
			var obj:Object = {id:id,name:name, address:address, phone:phone, email:email};
			customersArray.insertAt(0,obj);
			customersDataArray.insertAt(0,id+" / "+name+" / "+address+" / "+phone+" / "+email);
			selectedCustomerID = 0;
			trace("customer added");
		}
		
		public static function SaveInvoice(invoiceNumber:String):void
		{
			var obj:Object = {
				id:customersArray[selectedCustomerID].id,
				name:customersArray[selectedCustomerID].name,
				address:customersArray[selectedCustomerID].address,
				phone:customersArray[selectedCustomerID].phone,
				email:customersArray[selectedCustomerID].email,
				invoiceNumber:invoiceNumber,
				invoiceTotal:CalculateInvoice().toFixed(2),
				invoiceitems:tempInvoiceArray,
				paid:false
			}

			invoicesArray.push(obj);
		}
		
		public static function SendEmail():void
		{
			trace("send email");
//			GmailConnect();
			
			///////////////////////////////////////
			var variables:URLVariables = new URLVariables();
			var invoice:String;
			var total:String = CalculateInvoice().toFixed(2);
			
			invoice = "<table style=\"border-collapse: collapse; width: 550px;\" border=\"2\">" +
				"<tbody>" +
				"<tr>" +
				"<td style=\"width: 250px;\">"+"Item"+"</td>" +
				"<td style=\"width:100px;\">"+"Price"+"</td>" +
				"<td style=\"width: 100px;\">"+"Quantity"+"</td>" +
				"<td style=\"width: 100px;\">"+"Total"+"</td>" +
				"</tr>";

			for(var i:int=0; i<tempInvoiceArray.length; i++)
			{
				invoice = invoice +
					"<tr>" +
					"<td style=\"width: 250px;\">"+tempInvoiceArray[i].item+"</td>" +
					"<td style=\"width:100px;\">"+tempInvoiceArray[i].price+"</td>" +
					"<td style=\"width: 100px;\">"+tempInvoiceArray[i].quantity+"</td>" +
					"<td style=\"width: 100px;\">"+tempInvoiceArray[i].amount.toFixed(2)+"</td>" +
					"</tr>";
			}
			invoice = invoice + "" +
				"<tr>" +
				"<td style=\"width: 250px;\">"+"TOTAL"+"</td>" +
				"<td style=\"width:100px;\">"+"&nbsp;"+"</td>" +
				"<td style=\"width: 100px;\">"+"&nbsp;"+"</td>" +
				"<td style=\"width: 100px;\">"+total+"</td>" +
				"</tr>" +
				"</tbody>" +
				"</table>";
			
			var msg:String = "Dear "+ customersArray[selectedCustomerID].name +", <br><br>"+
				"Please pay your bill of total amount of"+ total + " USD to the below wallet address: <br><br>" +
				selectedWalletAddress +"<br><br>" +
				"this amount as a result of the below invoice:<br><br>" +
				invoice +"<br>"+
				"<br>Thank you for shoping from our store";
			variables.to = customersArray[selectedCustomerID].email;
			variables.from = MERCHANDISER_NAME;
			variables.subject = "Pay with crypto";
			variables.body = msg;
			
			var request:URLRequest = new URLRequest(emailSenderURL);
			request.data = variables;
			request.method = URLRequestMethod.POST;
			
			var urlLoader:URLLoader = new URLLoader();
			urlLoader.addEventListener(Event.COMPLETE,Sent);
			urlLoader.load(request);
			
			function Sent(e:Event):void
			{
				trace(e.target.data);
				e.target.removeEventListener(Event.COMPLETE,Sent);
				e.target.close();
			}			
		}
		
		
		
		public static function Paid():void
		{
			invoicesArray[DataHandler.invoicesArray.length-1].paid = true;
//			SendTransactionMarketerFee();
			selectedCustomerID = -1;
			tempInvoiceArray = [];
			mainView.customerPanel.Clear();
			isCurrentDataSaved = false;
		}
		
		private static function SendTransactionMarketerFee():void
		{
			// TODO Auto Generated method stub
			var urlRequest:URLRequest = new URLRequest("https://api.coinbase.com/v2/accounts");
			urlRequest.method = URLRequestMethod.GET;
			
			
			var urlRequestHeader_Authorization:URLRequestHeader = new URLRequestHeader("Authorization","Bearer "+coinbaseAccessToken); 
			urlRequest.requestHeaders.push(urlRequestHeader_Authorization);
			var urlRequestHeader_CBVERSION:URLRequestHeader = new URLRequestHeader("CB-VERSION","2018-02-16"); 
			urlRequest.requestHeaders.push(urlRequestHeader_CBVERSION);
			
			var urlLoader:URLLoader = new URLLoader();
			urlLoader.addEventListener(Event.COMPLETE, onURLLoaderComplete);
			urlLoader.load(urlRequest);
			function onURLLoaderComplete(event:Event):void
			{
				trace(event.target.data);
			}
		}
		
		public static function ExportData():void
		{
			var csv:String = "";
			csv += "Name,";
			csv += "Address,";
			csv += "Phone,";
			csv += "email,";
			csv += "Invoice Number,";
			csv += "Invoice Amount,";
			csv += "\n";
			for(var i:int=0; i<invoicesArray.length;i++)
			{
				csv += invoicesArray[i].name + ",";
				csv += invoicesArray[i].address + ",";
				csv += invoicesArray[i].phone + ",";
				csv += invoicesArray[i].email + ",";
				csv += invoicesArray[i].invoiceNumber + ",";
				csv += invoicesArray[i].invoiceTotal + ",";
				csv += "\n";
			}
			var file:FileReference = new FileReference();
			var bytes:ByteArray = new ByteArray();
			bytes.writeUTFBytes(csv);
			file.save(bytes,"data.csv");
		}
		public static function ClearData():void
		{
			var sharedObject:SharedObject = SharedObject.getLocal(SHARED_OBJECT);
			sharedObject.clear();
			selectedWalletID = 0;
			selectedWalletName = "";
			selectedWalletAddress = "";
			walletsArray = new Array;
			customersArray = new Array;
			customersDataArray = new Array;
			invoicesArray = new Array;
		}

		public static function get isCurrentDataSaved():Boolean
		{
			return _isCurrentDataSaved;
		}

		public static function set isCurrentDataSaved(value:Boolean):void
		{
			_isCurrentDataSaved = value;
		}
		
		public static function  GmailConnect():void
		{
			//			var auth:OAuth2
			var oauth2:OAuth2 = new OAuth2("https://accounts.google.com/o/oauth2/auth", "https://accounts.google.com/o/oauth2/token", LogSetupLevel.ALL);
			
			if(!isGmailConnected)
			{
				trace("connect Gmail");
				var browser:WebViewPanel = new WebViewPanel("Connect to Gmail to sending mails permission");
				browser.width = mainView.width;
				browser.height = mainView.height;;
				mainView.addChild(browser);				
				// get new accesstoken call
				var grant:IGrantType = new AuthorizationCodeGrant(browser.stageWebView,
					"685971579332-dkqevi4hr6bcv5rtbq1vgqpeabtc4keg.apps.googleusercontent.com",
					"dbIMEPzAzB0xRc7RvRJz2FFN",
					"urn:ietf:wg:oauth:2.0:oob",
//					"http://localhost",
					"https://www.googleapis.com/auth/gmail.send",
					null,
					null);
				oauth2.addEventListener(GetAccessTokenEvent.TYPE, onGetAccessToken);
				oauth2.getAccessToken(grant);
			}
			else
			{
				trace("refresh");
//				mainView.CoinbaseBtnRefreshing();
//				mainView.balanceBoard.RefreshBalance();
				oauth2.addEventListener(RefreshAccessTokenEvent.TYPE,onRefreshAccessToken);
				oauth2.refreshAccessToken(gmailRefreshToken,
					"685971579332-dkqevi4hr6bcv5rtbq1vgqpeabtc4keg.apps.googleusercontent.com",
					"dbIMEPzAzB0xRc7RvRJz2FFN")
			}
			function onGetAccessToken(getAccessTokenEvent:GetAccessTokenEvent):void
			{
				browser.removeFromParent(true);
				browser.stageWebView.dispose();
				if (getAccessTokenEvent.errorCode == null && getAccessTokenEvent.errorMessage == null)
				{
					// success!
					trace("getAccessToken success");
					var accessToken:String = getAccessTokenEvent.accessToken;
					trace("Your access token value is: " +accessToken);		
					trace("Your access token type is: " +getAccessTokenEvent.type);	
					trace("Your access token expires in: " +getAccessTokenEvent.expiresIn);	
					trace("Your refresh token value is: " +getAccessTokenEvent.refreshToken);	
					gmailAccessToken = accessToken;
					gmailRefreshToken = getAccessTokenEvent.refreshToken;
					GmailAPICall(accessToken);
				}
				else
				{
					// fail :(
					trace("getAccessToken failed");
				}				
			} 
			function onRefreshAccessToken(refreshAccessTokenEvent:RefreshAccessTokenEvent):void
			{
				if (refreshAccessTokenEvent.errorCode == null && refreshAccessTokenEvent.errorMessage == null)
				{
					// success!
					trace("refreshAccessToken success");
					var accessToken:String = refreshAccessTokenEvent.accessToken;
					trace("Your access token value is: " +accessToken);		
					trace("Your access token type is: " +refreshAccessTokenEvent.type);	
					trace("Your access token expires in: " +refreshAccessTokenEvent.expiresIn);	
					trace("Your refresh token value is: " +gmailRefreshToken);	
					gmailAccessToken = accessToken;
//					gmailRefreshToken = refreshAccessTokenEvent.refreshToken;
					GmailAPICall(accessToken);
				}
				else
				{
					// fail :(
					trace("refreshAccessToken failed");
				}
			}
		}
		
		private static function GmailAPICall(accessToken:String):void
		{
			isGmailConnected = true;
			
			// set up URL request
			var urlRequest:URLRequest = new URLRequest("https://www.googleapis.com/gmail/v1/users");
			urlRequest.method = URLRequestMethod.POST;
		}
		
		public static function  CheckCoinBase(walletPanel:WalletPanel):void
		{
			var _walletPanel:WalletPanel = walletPanel;
			//			var auth:OAuth2
			var oauth2:OAuth2 = new OAuth2("https://www.coinbase.com/oauth/authorize", "https://api.coinbase.com/oauth/token", LogSetupLevel.ALL);

			if(!isCoinbaseConnected)
			{
				var browser:WebViewPanel = new WebViewPanel("Connect to Coinbase Wallet");
				browser.width = mainView.width;
				browser.height = mainView.height;;
				mainView.addChild(browser);				
				// get new accesstoken call
				var grant:IGrantType = new AuthorizationCodeGrant(browser.stageWebView,
					"de4fd6ed7d42c64d8abb9a7f778152403c191ffdf0766cfd3031717b6988dadf",
					"1e863dfa5b59832bb4169d704e820ed3b35f66f9d770fc70d7fb21698d777365",
					"urn:ietf:wg:oauth:2.0:oob",
					"wallet:accounts:read,wallet:addresses:read,wallet:transactions:send&meta[send_limit_amount]=1&meta[send_limit_currency]=USD&meta[send_limit_period]=day",
					null,
					{account:"all"});
				oauth2.addEventListener(GetAccessTokenEvent.TYPE, onGetAccessToken);
				oauth2.getAccessToken(grant);
			}
			else
			{
				trace("refresh");
				_walletPanel.CoinbaseBtnRefreshing();
//				mainView.balanceBoard.RefreshBalance();
				oauth2.addEventListener(RefreshAccessTokenEvent.TYPE,onRefreshAccessToken);
				oauth2.refreshAccessToken(coinbaseRefreshToken,
					"de4fd6ed7d42c64d8abb9a7f778152403c191ffdf0766cfd3031717b6988dadf",
					"1e863dfa5b59832bb4169d704e820ed3b35f66f9d770fc70d7fb21698d777365")
			}

			
			function onGetAccessToken(getAccessTokenEvent:GetAccessTokenEvent):void
			{
				browser.removeFromParent(true);
				browser.stageWebView.dispose();
				if (getAccessTokenEvent.errorCode == null && getAccessTokenEvent.errorMessage == null)
				{
					// success!
					var accessToken:String = getAccessTokenEvent.accessToken;
					trace("Your access token value is: " +accessToken);		
					trace("Your access token type is: " +getAccessTokenEvent.type);	
					trace("Your access token expires in: " +getAccessTokenEvent.expiresIn);	
					trace("Your refresh token value is: " +getAccessTokenEvent.refreshToken);	
					coinbaseAccessToken = accessToken;
					coinbaseRefreshToken = getAccessTokenEvent.refreshToken;
					CoinbaseAPICall(accessToken, _walletPanel);
				}
				else
				{
					// fail :(
					trace("getAccessToken failed");
				}
			} 
			function onRefreshAccessToken(refreshAccessTokenEvent:RefreshAccessTokenEvent):void
			{
				if (refreshAccessTokenEvent.errorCode == null && refreshAccessTokenEvent.errorMessage == null)
				{
					// success!
					var accessToken:String = refreshAccessTokenEvent.accessToken;
					trace("Your access token value is: " +accessToken);		
					trace("Your access token type is: " +refreshAccessTokenEvent.type);	
					trace("Your access token expires in: " +refreshAccessTokenEvent.expiresIn);	
					trace("Your refresh token value is: " +refreshAccessTokenEvent.refreshToken);	
					coinbaseAccessToken = accessToken;
					coinbaseRefreshToken = refreshAccessTokenEvent.refreshToken;
					coinbaseDataArray = [];
					coinbaseCodesIndexArray = [];
					coinbaseWalletsArray = [];
					CoinbaseAPICall(accessToken, _walletPanel);
				}
				else
				{
					// fail :(
					trace("refreshAccessToken failed");
				}
			} 
			
		}
		

		
		private static function CoinbaseAPICall(accessToken:String, walletPanel:WalletPanel):void
		{
			var _walletPanel:WalletPanel = walletPanel;
			// set up URL request
			var urlRequest:URLRequest = new URLRequest("https://api.coinbase.com/v2/accounts");
			urlRequest.method = URLRequestMethod.GET;
			

			var urlRequestHeader_Authorization:URLRequestHeader = new URLRequestHeader("Authorization","Bearer "+accessToken); 
			urlRequest.requestHeaders.push(urlRequestHeader_Authorization);
			var urlRequestHeader_CBVERSION:URLRequestHeader = new URLRequestHeader("CB-VERSION","2018-02-16"); 
			urlRequest.requestHeaders.push(urlRequestHeader_CBVERSION);
			
			var urlLoader:URLLoader = new URLLoader();
			urlLoader.addEventListener(Event.COMPLETE, onURLLoaderComplete);
			urlLoader.load(urlRequest);
			
			function onURLLoaderComplete(event:Event):void
			{
				var obj:Object = JSON.parse(event.target.data);
				trace("event.target.data "+ event.target.data);
				var BTC:String;
				var ETH:String;
				var BCH:String;
				var LTC:String;
				var dataArray:Array = obj.data;
				for each(var item:Object in dataArray)
				{
					trace("id: "+item.id+" name "+item.name+" balance: "+item.balance.amount+" "+ item.currency.address_regex);
//					var regex:RegExp = RegExp(item.currency.address_regex);
//					var addressStr:String = regex.source;
//					trace("address "+addressStr);
					var dataObj:Object = {name:item.name, primary:item.primary, currency:item.currency.name,
						code:item.currency.code, color:item.currency.color, balance:item.balance.amount, resource_path:item.resource_path};
					coinbaseDataArray.push(dataObj);
					coinbaseCodesIndexArray.push(item.currency.code);
					
					switch(item.currency.code)
					{
						case "LTC":
							LTC = item.balance.amount;
							break;
						case "ETH":
							ETH = item.balance.amount;
							break;
						case "BCH":
							BCH = item.balance.amount;
							break;
						case "BTC":
							BTC = item.balance.amount;
							break;
					}
				}
				isCoinbaseConnected = true;
				getCoinbaseWalletAddress(accessToken, _walletPanel);
				_walletPanel.CoinbaseBtnLabel();

//				mainView.balanceBoard.UpdateBalances(BTC,ETH,BCH,LTC);
			}
			
		}
		
		private static function getCoinbaseWalletAddress(accessToken:String, walletPanel:WalletPanel):void
		{
			var _walletPanel:WalletPanel = walletPanel;
			for(var i:int=0; i<coinbaseDataArray.length; i++)
			{
				var urlRequest:URLRequest = new URLRequest("https://api.coinbase.com"+coinbaseDataArray[i].resource_path+"/addresses");
				urlRequest.method = URLRequestMethod.GET;
				
				
				var urlRequestHeader_Authorization:URLRequestHeader = new URLRequestHeader("Authorization","Bearer "+accessToken); 
				urlRequest.requestHeaders.push(urlRequestHeader_Authorization);
				var urlRequestHeader_CBVERSION:URLRequestHeader = new URLRequestHeader("CB-VERSION","2018-02-16"); 
				urlRequest.requestHeaders.push(urlRequestHeader_CBVERSION);
				
				var urlLoader:URLLoader = new URLLoader();
				urlLoader.addEventListener(Event.COMPLETE, onURLLoaderComplete);
				urlLoader.load(urlRequest);
				function onURLLoaderComplete(event:Event):void
				{
					var obj:Object = JSON.parse(event.target.data);
//					trace("adresses event.target.data "+ event.target.data);
					for(var o:int=0; o<obj.data.length; o++)
					{
						var walletName:String;
						switch(obj.data[o].network)
						{
							case "litecoin":
								walletName = coinbaseDataArray[coinbaseCodesIndexArray.indexOf("LTC")].name
								break;
							case "ethereum":
								walletName = coinbaseDataArray[coinbaseCodesIndexArray.indexOf("ETH")].name
								break;
							case "bitcoin_cash":
								walletName = coinbaseDataArray[coinbaseCodesIndexArray.indexOf("BCH")].name
								break;
							case "bitcoin":
								walletName = coinbaseDataArray[coinbaseCodesIndexArray.indexOf("BTC")].name
								break;
						}
						var nameStr:String = "Coinbase - "+walletName+" - "+obj.data[o].name;
						var itemObj:Object = {walletName:walletName,
							name:nameStr,
							address:obj.data[o].address,
							text:nameStr+": "+obj.data[o].address};
						coinbaseWalletsArray.push(itemObj);
							
					}
					_walletPanel.UpdateWalletsList();
				}
			}
		}


		public static function Wallets():Array
		{
			var wallets:Array = new Array;
			trace("walletsArray.length "+walletsArray.length);
			wallets = walletsArray.concat(coinbaseWalletsArray);
			trace("walletsArray.length "+walletsArray.length);
			trace("wallets.length "+wallets.length);
			return wallets;
		}

		public static function get isCoinbaseConnected():Boolean
		{
			return _isCoinbaseConnected;
		}

		public static function set isCoinbaseConnected(value:Boolean):void
		{
			_isCoinbaseConnected = value;
		}

		public static function get isGmailConnected():Boolean
		{
			return _isGmailConnected;
		}

		public static function set isGmailConnected(value:Boolean):void
		{
			_isGmailConnected = value;
		}
		public static function ChangeMarketerPass(pass:String):void
		{
			marketerPass = pass;
		}

	}
}