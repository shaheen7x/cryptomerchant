package screens
{
	import assets.Assets;
	
	import data.DataHandler;
	
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.TextInput;
	import feathers.core.FeathersControl;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalAlign;
	import feathers.layout.VerticalLayout;
	
	import starling.display.Image;
	import starling.events.Event;
	
	public class MarketerPanel extends FeathersControl
	{
		private var layoutVerTopCenter:VerticalLayout;
		private var mainCont:LayoutGroup;
		private var header:Header;
		private var marketerBtn:Button;
		private var layoutHorMedLeft:HorizontalLayout;
		private var inputCont:LayoutGroup;
		private var feesInput:TextInput;
		private var saveBtn:Button;
		private var feesLabel:Label;
		public function MarketerPanel()
		{
			super();
		}
		override protected function initialize():void
		{
			layoutVerTopCenter = new VerticalLayout;
			layoutVerTopCenter.verticalAlign =  VerticalAlign.TOP;
			layoutVerTopCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutHorMedLeft = new HorizontalLayout;
			layoutHorMedLeft.verticalAlign =  VerticalAlign.MIDDLE;
			layoutHorMedLeft.horizontalAlign = HorizontalAlign.CENTER;
			
			mainCont = new LayoutGroup;
			mainCont.layout = layoutVerTopCenter;			
			
			header = new Header;
			header.title = "Marketer Settings";
			
			inputCont = new LayoutGroup;
			inputCont.layout = layoutHorMedLeft;
			
			feesLabel = new Label;
			feesLabel.text = "Fees:";
			
			feesInput = new TextInput;
			feesInput.restrict = "0-9.";
			feesInput.text = DataHandler.fees.toFixed(2);
			
			saveBtn = new Button;
			saveBtn.defaultIcon = new Image(Assets.assetsManager.getTexture("save"));
			saveBtn.addEventListener(Event.TRIGGERED, onSaveBtnTriggered);
			inputCont.addChild(feesLabel);
			inputCont.addChild(feesInput);
			inputCont.addChild(saveBtn);
			
			
			marketerBtn = new Button;
			marketerBtn.label = "Change Lock Key";
			marketerBtn.addEventListener(Event.TRIGGERED, onMarketerBtnTriggered);
			
			mainCont.addChild(header);
			mainCont.addChild(inputCont);
			mainCont.addChild(marketerBtn);
			this.addChild(mainCont);
		}
		
		private function onSaveBtnTriggered(e:Event):void
		{
			if(feesInput.text!="" )
			{
				DataHandler.fees = Number(feesInput.text);
			}			
		}
		
		private function onMarketerBtnTriggered():void
		{
			this.dispatchEventWith(Event.TRIGGERED);
		}
		override protected function draw():void
		{	
			layoutVerTopCenter.gap = 20;
			
			mainCont.setSize(actualWidth,actualHeight);			
			header.setSize(actualWidth, actualHeight * 0.2);
			marketerBtn.setSize(actualWidth * 0.2, 40);
			layoutHorMedLeft.gap = 5;
			inputCont.setSize(actualWidth * 0.9, 40);
			feesLabel.setSize(50,20);
			feesInput.setSize(inputCont.width * 0.2, 35);
			saveBtn.setSize(35,35);

			//			buttonsGroup.paddingTop = 30;
		}
	}
}