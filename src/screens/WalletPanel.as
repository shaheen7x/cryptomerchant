package screens
{
	import assets.Assets;
	
	import data.DataHandler;
	
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.List;
	import feathers.controls.TextInput;
	import feathers.core.FeathersControl;
	import feathers.data.ArrayCollection;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalAlign;
	import feathers.layout.VerticalLayout;
	
	import starling.display.Image;
	import starling.events.Event;
	
	public class WalletPanel extends FeathersControl
	{
		private var layoutVerTopCenter:VerticalLayout;
		private var layoutVerMedCenter:VerticalLayout;
		private var layoutHorMedCenter:HorizontalLayout;
		private var mainCont:LayoutGroup;
		private var header:Header;
		private var newWalletLabel:Label;
		private var selectWalletLabel:Label;
		private var layoutVerMedLeft:VerticalLayout;
		private var subCont:LayoutGroup;
		private var layoutVerTopLeft:VerticalLayout;
		private var inputCont:LayoutGroup;
		private var walletNameInput:TextInput;
		private var walletAddressInput:TextInput;
		private var saveBtn:Button;
		private var walletsList:List;
		private var layoutHorMedLeft:HorizontalLayout;
		private var coinbaseBtn:Button;
		
		public function WalletPanel()
		{
			super();
		}
		override protected function initialize():void
		{
			layoutVerTopCenter = new VerticalLayout;
			layoutVerTopCenter.verticalAlign =  VerticalAlign.TOP;
			layoutVerTopCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutVerMedCenter = new VerticalLayout;
			layoutVerMedCenter.verticalAlign =  VerticalAlign.MIDDLE;
			layoutVerMedCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutVerTopLeft = new VerticalLayout;
			layoutVerTopLeft.verticalAlign =  VerticalAlign.TOP;
			layoutVerTopLeft.horizontalAlign = HorizontalAlign.LEFT;
			
			layoutHorMedCenter = new HorizontalLayout;
			layoutHorMedCenter.verticalAlign =  VerticalAlign.MIDDLE;
			layoutHorMedCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutHorMedLeft = new HorizontalLayout;
			layoutHorMedLeft.verticalAlign =  VerticalAlign.MIDDLE;
			layoutHorMedLeft.horizontalAlign = HorizontalAlign.LEFT;
			
			mainCont = new LayoutGroup;
			mainCont.layout = layoutVerTopCenter;

			
			header = new Header;
			header.title = "Wallets Settings";
			
			subCont = new LayoutGroup;
			subCont.layout = layoutVerTopLeft;
			
			newWalletLabel = new Label;
			newWalletLabel.text = "Add new Wallet:";
			newWalletLabel.paddingTop = 15;
			
			inputCont = new LayoutGroup;
			inputCont.layout = layoutHorMedLeft;
			walletNameInput = new TextInput;
			walletNameInput.prompt = "Enter Wallet Name!";
			walletAddressInput = new TextInput;
			walletAddressInput.prompt = "Enter Wallet Address!";
			saveBtn = new Button;
			saveBtn.defaultIcon = new Image(Assets.assetsManager.getTexture("save"));
			saveBtn.addEventListener(Event.TRIGGERED, onSaveBtnTriggered);
			inputCont.addChild(walletNameInput);
			inputCont.addChild(walletAddressInput);
			inputCont.addChild(saveBtn);			
			
			selectWalletLabel = new Label;
			selectWalletLabel.text = "Select a Wallet:";
			selectWalletLabel.paddingTop = 15;
			
			walletsList = new List;
			UpdateWalletsList();
			walletsList.addEventListener(Event.CHANGE, onListSelected);
			
			subCont.addChild(newWalletLabel);
			subCont.addChild(inputCont);
			subCont.addChild(selectWalletLabel);
			subCont.addChild(walletsList);
			
			coinbaseBtn = new Button;
			CoinbaseBtnLabel();
			coinbaseBtn.addEventListener(Event.TRIGGERED, onCoinBaseBtn);
			
			mainCont.addChild(header);
			mainCont.addChild(subCont);
			mainCont.addChild(coinbaseBtn);
			this.addChild(mainCont);
		}
		
		private function onCoinBaseBtn(e:Event):void
		{
			DataHandler.CheckCoinBase(this);
		}
		public function CoinbaseBtnLabel():void
		{
			trace("DataHandler.isCoinbaseConnected "+DataHandler.isCoinbaseConnected);
			coinbaseBtn.isEnabled = true;
			coinbaseBtn.label = (!DataHandler.isCoinbaseConnected)? "CoinBase - Connect" : "CoinBase - Refresh";
		}
		public function CoinbaseBtnRefreshing():void
		{
			coinbaseBtn.isEnabled = false;
			coinbaseBtn.label = "Refreshing...";		
		}
		public function CoinbaseBtnRefreshed():void
		{
			coinbaseBtn.isEnabled = true;
			coinbaseBtn.label = "CoinBase - Refresh";		
		}
		
		private function onListSelected(e:Event):void
		{
			if(walletsList.selectedIndex!=-1)
			{
				DataHandler.SelectWallet(walletsList.selectedIndex);
			}
		}
		public function UpdateWalletsList():void
		{
			var listData:ArrayCollection = new ArrayCollection(DataHandler.Wallets());
			walletsList.dataProvider = listData;
			walletsList.itemRendererProperties.labelField = "text";
			if(DataHandler.walletsArray.length>0)
			{
				walletsList.selectedIndex = DataHandler.selectedWalletID;
			}else{
				walletsList.selectedIndex = -1;
			}
			
		}
		private function onSaveBtnTriggered(e:Event):void
		{
			if(walletNameInput.text!="" && walletAddressInput.text!=="")
			{
				DataHandler.AddNewWallet(walletNameInput.text, walletAddressInput.text);
				UpdateWalletsList();
			}			
		}
		override protected function draw():void
		{	
			layoutHorMedCenter.gap = 6;
			layoutHorMedLeft.gap = 6;
			layoutVerTopCenter.gap = 6;
			
			mainCont.setSize(actualWidth,actualHeight);
			
			header.setSize(actualWidth, actualHeight * 0.1);
			subCont.setSize(actualWidth, (actualHeight * 0.89) - 48);
			
			newWalletLabel.setSize(120, 35);
			inputCont.setSize(actualWidth * 0.9, 40);
			walletNameInput.setSize(inputCont.width * 0.2, 35);
			walletAddressInput.setSize(inputCont.width * 0.6, 35);
			saveBtn.setSize(35,35);
			selectWalletLabel.setSize(120, 35);
			walletsList.setSize(actualWidth * 0.5, 80);
			coinbaseBtn.setSize(150,40);
			
		}
	}
}