package screens
{
	import assets.Assets;
	
	import data.DataHandler;
	
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.TextInput;
	import feathers.core.FeathersControl;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalAlign;
	import feathers.layout.VerticalLayout;
	
	import starling.display.Image;
	import starling.events.Event;
	
	public class EmailSettingsPanel extends FeathersControl
	{
		private var layoutVerTopCenter:VerticalLayout;
		private var layoutVerMedCenter:VerticalLayout;
		private var layoutVerTopLeft:VerticalLayout;
		private var layoutHorMedCenter:HorizontalLayout;
		private var layoutHorMedLeft:HorizontalLayout;
		private var mainCont:LayoutGroup;
		private var header:Header;
		private var emailURLLabel:Label;
		private var subCont:LayoutGroup;
		private var inputCont:LayoutGroup;
		private var emailURLInput:TextInput;
		private var saveBtn:Button;
		public function EmailSettingsPanel()
		{
			super();
		}
		override protected function initialize():void
		{
			layoutVerTopCenter = new VerticalLayout;
			layoutVerTopCenter.verticalAlign =  VerticalAlign.TOP;
			layoutVerTopCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutVerMedCenter = new VerticalLayout;
			layoutVerMedCenter.verticalAlign =  VerticalAlign.MIDDLE;
			layoutVerMedCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutVerTopLeft = new VerticalLayout;
			layoutVerTopLeft.verticalAlign =  VerticalAlign.TOP;
			layoutVerTopLeft.horizontalAlign = HorizontalAlign.LEFT;
			
			layoutHorMedCenter = new HorizontalLayout;
			layoutHorMedCenter.verticalAlign =  VerticalAlign.MIDDLE;
			layoutHorMedCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutHorMedLeft = new HorizontalLayout;
			layoutHorMedLeft.verticalAlign =  VerticalAlign.MIDDLE;
			layoutHorMedLeft.horizontalAlign = HorizontalAlign.LEFT;
			
			mainCont = new LayoutGroup;
			mainCont.layout = layoutVerTopCenter;
			
			
			header = new Header;
			header.title = "Email Sending Settings";
			
			subCont = new LayoutGroup;
			subCont.layout = layoutVerTopLeft;
			
			emailURLLabel = new Label;
			emailURLLabel.text = "Email sending server URL:";
			emailURLLabel.paddingTop = 15;
			
			inputCont = new LayoutGroup;
			inputCont.layout = layoutHorMedLeft;
			
			emailURLInput = new TextInput;
			emailURLInput.prompt = "Enter the full URL of your email sender php file!";
			if(DataHandler.emailSenderURL !="" && DataHandler.emailSenderURL != null)
				emailURLInput.text = DataHandler.emailSenderURL;
			
			saveBtn = new Button;
			saveBtn.defaultIcon = new Image(Assets.assetsManager.getTexture("save"));
			saveBtn.addEventListener(Event.TRIGGERED, onSaveBtnTriggered);
			inputCont.addChild(emailURLInput);
			inputCont.addChild(saveBtn);
			
			subCont.addChild(emailURLLabel);
			subCont.addChild(inputCont);

			
			mainCont.addChild(header);
			mainCont.addChild(subCont);
			this.addChild(mainCont);
		}
		private function onSaveBtnTriggered(e:Event):void
		{
			if(emailURLInput.text!="" )
			{
				DataHandler.emailSenderURL = emailURLInput.text;
			}			
		}
		override protected function draw():void
		{	
			layoutHorMedCenter.gap = 6;
			layoutHorMedLeft.gap = 6;
			
			mainCont.setSize(actualWidth,actualHeight);
			
			header.setSize(actualWidth, actualHeight * 0.2);
			subCont.setSize(actualWidth, actualHeight * 0.89);
			
			emailURLLabel.setSize(300, 35);
			inputCont.setSize(actualWidth * 0.9, 40);
			emailURLInput.setSize(inputCont.width * 0.8, 35);
			saveBtn.setSize(35,35);
			
		}
	}
}