package screens
{
	import com.greensock.easing.Expo;
	
	import assets.Assets;
	
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.LayoutGroup;
	import feathers.core.FeathersControl;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.VerticalAlign;
	import feathers.layout.VerticalLayout;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	
	public class SettingsPanel extends FeathersControl
	{
		private var layoutVerTopCenter:VerticalLayout;
		private var mainCont:LayoutGroup;
		private var header:Header;
		private var closeBtn:Button;
		private var walletPanel:WalletPanel;
		private var emailSettingsPanel:EmailSettingsPanel;
		private var exportPanel:ExportPanel;
		private var marketerPanel:MarketerPanel;
		private var marketerScreen:MarketerScreen;
		private var passwordpanel:PasswordPanel;
		
		public function SettingsPanel()
		{
			super();
		}
		override protected function initialize():void
		{
			layoutVerTopCenter = new VerticalLayout;
			layoutVerTopCenter.verticalAlign =  VerticalAlign.TOP;
			layoutVerTopCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			mainCont = new LayoutGroup;
			mainCont.layout = layoutVerTopCenter;
			mainCont.backgroundSkin = new Quad(2,2,0x333333);
			
			header = new Header;
			header.title = "Settings";			
			closeBtn = new Button;
			closeBtn.defaultIcon = new Image(Assets.assetsManager.getTexture("close"));
			closeBtn.addEventListener(Event.TRIGGERED, onBtnCloseTriggered);
			header.rightItems = new Vector.<DisplayObject>;
			header.rightItems.push(closeBtn);
			
			walletPanel = new WalletPanel;
			emailSettingsPanel = new EmailSettingsPanel;
			exportPanel = new ExportPanel;
			marketerPanel = new MarketerPanel;
			marketerPanel.addEventListener(Event.TRIGGERED,onMarketerTriggered);
			
			mainCont.addChild(header);
			mainCont.addChild(walletPanel);
			mainCont.addChild(marketerPanel);
			mainCont.addChild(emailSettingsPanel);
			mainCont.addChild(exportPanel);			
			this.addChild(mainCont);
		}
		
		private function onMarketerTriggered():void
		{
			passwordpanel = new PasswordPanel(true);
			this.addChild(passwordpanel);
			passwordpanel.setSize(actualWidth,actualHeight);
			passwordpanel.validate();
		}
		override protected function draw():void
		{	
			layoutVerTopCenter.gap = 6;
			mainCont.setSize(actualWidth,actualHeight);
			
			header.setSize(actualWidth, actualHeight * 0.1);
			closeBtn.setSize(40,40);
			walletPanel.setSize(actualWidth, actualHeight * 0.36);
			emailSettingsPanel.setSize(actualWidth, actualHeight * 0.12);
			exportPanel.setSize(actualWidth, actualHeight * 0.12);
			marketerPanel.setSize(actualWidth, actualHeight * 0.22);

		}
		
		private function onBtnCloseTriggered():void
		{
			this.removeFromParent(true);
		}
	}
}