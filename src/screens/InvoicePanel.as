package screens
{
	import assets.Assets;
	
	import data.DataHandler;
	
	import feathers.controls.Alert;
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.ScrollContainer;
	import feathers.controls.ScrollPolicy;
	import feathers.controls.TextInput;
	import feathers.core.FeathersControl;
	import feathers.data.ArrayCollection;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalAlign;
	import feathers.layout.VerticalLayout;
	
	import objects.InvoiceLine;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	
	public class InvoicePanel extends FeathersControl
	{
		private var layoutVerMedCenter:VerticalLayout;
		private var mainCont:LayoutGroup;
		private var layoutHorMedCenter:HorizontalLayout;
		private var inputCont:LayoutGroup;
		private var invoiceCont:ScrollContainer;
		private var invoiceNumInput:TextInput;
		private var itemLabel:Label;
		private var itemInput:TextInput;
		private var priceLabel:Label;
		private var priceInput:TextInput;
		private var quantityLabel:Label;
		private var quantityInput:TextInput;
		
		private var addBtn:Button;
		private var header:Header;
		private var layoutVerTopCenter:VerticalLayout;
		
		private var invoiceHeader:InvoiceLine;
		private var invoiceTotal:InvoiceLine;
		private var invoiceLine:InvoiceLine;
		private var headerSeperator:Quad;
		private var totalSeperator:Quad;
		private var invoiceControlCont:LayoutGroup;
		private var newBtn:Button;
		private var saveBtn:Button;
		private var invoiceNumlabel:Label;
		private var isFeeAdded:Boolean = false;
		
		public function InvoicePanel()
		{
			super();
		}
		override protected function initialize():void
		{
			layoutVerTopCenter = new VerticalLayout;
			layoutVerTopCenter.verticalAlign =  VerticalAlign.TOP;
			layoutVerTopCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutVerMedCenter = new VerticalLayout;
			layoutVerMedCenter.verticalAlign =  VerticalAlign.MIDDLE;
			layoutVerMedCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutHorMedCenter = new HorizontalLayout;
			layoutHorMedCenter.verticalAlign =  VerticalAlign.MIDDLE;
			layoutHorMedCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			mainCont = new LayoutGroup;
			mainCont.layout = layoutVerTopCenter;
			
			header = new Header;
			header.title = "Invoice Data";
			
			invoiceControlCont = new LayoutGroup;
			invoiceControlCont.layout = layoutHorMedCenter;
			newBtn = new Button;
			newBtn.label = "New";
			newBtn.addEventListener(Event.TRIGGERED, onNewBtnTriggered);
			invoiceNumlabel = new Label;
			invoiceNumlabel.text = "Invoice Number:";
			invoiceNumlabel.paddingTop = 7;
			invoiceNumInput = new TextInput;
			invoiceNumInput.restrict = "0-9";
			saveBtn = new Button;
			saveBtn.defaultIcon = new Image(Assets.assetsManager.getTexture("save"));
			saveBtn.addEventListener(Event.TRIGGERED, onSaveBtnTriggered);
			
//			invoiceControlCont.addChild(newBtn);
			invoiceControlCont.addChild(invoiceNumlabel);
			invoiceControlCont.addChild(invoiceNumInput);
//			invoiceControlCont.addChild(saveBtn);
			
			inputCont = new LayoutGroup;
			inputCont.layout = layoutHorMedCenter;
			
			invoiceCont = new ScrollContainer;
			invoiceCont.layout = layoutVerTopCenter;
			invoiceCont.backgroundSkin = new Quad(2,2,0x565656);
			invoiceCont.horizontalScrollPolicy = ScrollPolicy.OFF;
			
			itemLabel = new Label;
			itemLabel.text = "Item:";
			itemInput = new TextInput;
			
			priceLabel = new Label;
			priceLabel.text = "Price:";
			priceInput = new TextInput;
			priceInput.restrict = "0-9.";
			
			quantityLabel = new Label;
			quantityLabel.text = "Quantity:";
			quantityInput = new TextInput;
			quantityInput.restrict = "0-9."
			
			addBtn = new Button;
			addBtn.defaultIcon = new Image(Assets.assetsManager.getTexture("plus"));
			addBtn.addEventListener(Event.TRIGGERED, onAddBtnTriggered);
			
			inputCont.addChild(itemLabel);
			inputCont.addChild(itemInput);
			inputCont.addChild(priceLabel);
			inputCont.addChild(priceInput);
			inputCont.addChild(quantityLabel);
			inputCont.addChild(quantityInput);
			inputCont.addChild(addBtn);
			
			invoiceHeader = new InvoiceLine("Item", "Price", "Quantity",0,"Amount");
			headerSeperator = new Quad(450,2,0xfcfcfc);
			totalSeperator = new Quad(450,2,0xfcfcfc);
			invoiceTotal = new InvoiceLine("TOTAL", "", "",0);
			
			invoiceCont.addChild(invoiceHeader);
			invoiceCont.addChild(headerSeperator);
			invoiceCont.addChild(totalSeperator);
			invoiceCont.addChild(invoiceTotal);
//			invoiceHeader.validate();
			
			mainCont.addChild(header);
			mainCont.addChild(invoiceControlCont);
			mainCont.addChild(inputCont);
			mainCont.addChild(invoiceCont);
			this.addChild(mainCont);
			
		}
		
		private function onNewBtnTriggered():void
		{
			// TODO Auto Generated method stub
			
		}
		
		private function onSaveBtnTriggered():void
		{
			SaveInvoiceData();			
		}
		public function SaveInvoiceData():void
		{
			var alert:Alert
			if(invoiceNumInput.text != "" && invoiceNumInput.text != null)
			{
				if(DataHandler.tempInvoiceArray.length != 0)
				{
					if(DataHandler.selectedCustomerID != -1)
					{
						DataHandler.SaveInvoice(invoiceNumInput.text);


					}else
					{
						alert = Alert.show( "Please select a customer data.", "Warning", new ArrayCollection(
							[{ label: "OK"}	]
						));
					}
				}else{
					alert = Alert.show( "Please enter at least one invoice item.", "Warning", new ArrayCollection(
						[{ label: "OK"}	]
					));
				}
			}else{
				alert = Alert.show( "Please fill in the Invoice Number.", "Warning", new ArrayCollection(
					[{ label: "OK"}	]
				));
			}
		}
		
		private function onAddBtnTriggered(e:Event):void
		{
			trace(invoiceCont.numChildren);
			var total:Number 
			if(!isFeeAdded)
			{
				invoiceLine = new InvoiceLine("Fees", DataHandler.fees.toFixed(2), "1");
				invoiceCont.addChildAt(invoiceLine,invoiceCont.numChildren-2);
				invoiceLine.setSize(actualWidth*0.9,30);
				invoiceLine.validate();
				total = DataHandler.CalculateInvoice("Fees",DataHandler.fees.toFixed(2), "1");
				trace("total "+total);
				invoiceTotal.UpdateVlaues("TOTAL", "", "",0,total.toFixed(2));
				invoiceTotal.validate();
				isFeeAdded = true;
			}
			if(itemInput.text!="" && priceInput.text!="" && quantityInput.text!="")
			{
				invoiceLine = new InvoiceLine(itemInput.text, priceInput.text, quantityInput.text);
				invoiceCont.addChildAt(invoiceLine,invoiceCont.numChildren-3);
				invoiceLine.setSize(actualWidth*0.9,30);
				invoiceLine.validate();
				total = DataHandler.CalculateInvoice(itemInput.text,priceInput.text,quantityInput.text);
				trace("total "+total);
				invoiceTotal.UpdateVlaues("TOTAL", "", "",0,total.toFixed(2));
				invoiceTotal.validate();
				itemInput.text = priceInput.text = quantityInput.text = ""
			}else{
				var alert:Alert = Alert.show( "Please fill in the empty invoice field.", "Warning", new ArrayCollection(
					[
						{ label: "OK"}
					]) );
			}

			
		}
		public function Clear():void
		{
			this.removeChildren(0,-1,true);
			isFeeAdded = false
			this.initialize();
			this.draw();

//			
//			invoiceNumInput.text = "";
//			itemInput.text = "";
//			priceInput.text = "";
//			quantityInput.text = "";
//			
//			invoiceCont.removeChildren();
//			invoiceCont.addChild(invoiceHeader);
//			invoiceCont.addChild(headerSeperator);
//			invoiceCont.addChild(totalSeperator);
//			invoiceCont.addChild(invoiceTotal);
//			invoiceHeader.validate();
			
		}
		
		override protected function draw():void
		{	
			layoutHorMedCenter.gap = 6;
			mainCont.setSize(actualWidth,actualHeight);	
			invoiceControlCont.setSize(actualWidth,actualHeight * 0.13);
			inputCont.setSize(actualWidth,actualHeight * 0.13);	
			invoiceCont.setSize(actualWidth,actualHeight * 0.6);	
			
			header.setSize(actualWidth, actualHeight * 0.1);
			trace(actualWidth);
			newBtn.setSize(60,35);
			invoiceNumlabel.setSize(100,35);
			invoiceNumInput.setSize(100,35);
			saveBtn.setSize(35,35);
			itemLabel.setSize(40, 35);
			itemInput.setSize(150, 35);
			priceLabel.setSize(50, 35);
			priceInput.setSize(60, 35);
			quantityLabel.setSize(65, 35);
			quantityInput.setSize(60, 35);
			addBtn.setSize(35,35);
			
			invoiceHeader.setSize(actualWidth *0.9,30);
			invoiceTotal.setSize(actualWidth *0.9,30);

		}
	}
}