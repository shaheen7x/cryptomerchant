package screens
{
	import assets.Assets;
	
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.LayoutGroup;
	import feathers.core.FeathersControl;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.VerticalAlign;
	import feathers.layout.VerticalLayout;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	
	public class MarketerScreen extends FeathersControl
	{
		private var layoutVerTopCenter:VerticalLayout;
		private var mainCont:LayoutGroup;
		private var header:Header;
		private var closeBtn:Button;
		private var passwordpanel:PasswordPanel;
		
		public function MarketerScreen()
		{
			super();
		}
		override protected function initialize():void
		{
			layoutVerTopCenter = new VerticalLayout;
			layoutVerTopCenter.verticalAlign =  VerticalAlign.TOP;
			layoutVerTopCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			mainCont = new LayoutGroup;
			mainCont.layout = layoutVerTopCenter;
			mainCont.backgroundSkin = new Quad(2,2,0x333333);
			
			header = new Header;
			header.title = "Marketer Settings";			
			closeBtn = new Button;
			closeBtn.defaultIcon = new Image(Assets.assetsManager.getTexture("close"));
			closeBtn.addEventListener(Event.TRIGGERED, onBtnCloseTriggered);
			header.rightItems = new Vector.<DisplayObject>;
			header.rightItems.push(closeBtn);
			
			passwordpanel = new PasswordPanel;
			
			mainCont.addChild(header);
			this.addChild(mainCont);
			this.addChild(passwordpanel);
		}
		
		private function onBtnCloseTriggered():void
		{
			this.removeFromParent(true);
		}
		override protected function draw():void
		{	
			layoutVerTopCenter.gap = 20;
			
			mainCont.setSize(actualWidth,actualHeight);			
			header.setSize(actualWidth, actualHeight * 0.08);
			closeBtn.setSize(40,40);
			passwordpanel.setSize(actualWidth , actualHeight);
		}
	}
}