package screens
{
	import data.DataHandler;
	
	import feathers.controls.Alert;
	import feathers.controls.Button;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.TextInput;
	import feathers.core.FeathersControl;
	import feathers.data.ArrayCollection;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.VerticalAlign;
	import feathers.layout.VerticalLayout;
	
	import starling.display.Quad;
	import starling.events.Event;
	
	public class PasswordPanel extends FeathersControl
	{
		private var layoutVerTopCenter:VerticalLayout;
		private var mainCont:LayoutGroup;
		private var label:Label;
		private var passInput:TextInput;
		private var passInputConfirm:TextInput;
		private var passBtn:Button;
		private var isChange:Boolean = false
		public function PasswordPanel(change:Boolean = false)
		{
			isChange = change;
			super();
		}
		override protected function initialize():void
		{
			layoutVerTopCenter = new VerticalLayout;
			layoutVerTopCenter.verticalAlign =  VerticalAlign.MIDDLE;
			layoutVerTopCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			mainCont = new LayoutGroup;
			mainCont.backgroundSkin = new Quad(2,2,0x333333);
			mainCont.layout = layoutVerTopCenter;
			
			label = new Label;
			label.text = (isChange)?"Change you password?" : "Enter your password:";
			
			passInput = new TextInput;
			passInput.displayAsPassword = true;
			
			passInputConfirm = new TextInput;
			passInputConfirm.displayAsPassword = true;
			
			passBtn = new Button;
			passBtn.label = (isChange) ? "Change" : "Login";
			passBtn.addEventListener(Event.TRIGGERED, onbtnTriggered);
			
			mainCont.addChild(label);
			mainCont.addChild(passInput);
			if(isChange) mainCont.addChild(passInputConfirm);
			mainCont.addChild(passBtn);
			
			this.addChild(mainCont);
		}
		
		private function onbtnTriggered():void
		{
			var alert:Alert = new Alert;
			if(isChange)
			{
				if(passInput.text == passInputConfirm.text)
				{
					DataHandler.ChangeMarketerPass(passInput.text);
					this.removeFromParent(true);
				}else{
					passInput.text = passInputConfirm.text = "";
					alert = Alert.show( "Password didn't match, Please input the password again!", "Warning", new ArrayCollection(
						[
							{ label: "OK"},
							{ label: "CANCEL", triggered: cancelButton_triggeredHandler }
						]) );
				}					
			}else{
				trace(passInput.text);
				trace(DataHandler.marketerPass);
				if(passInput.text == DataHandler.marketerPass)
				{
					this.removeFromParent();
					passInput.text = "";
					DataHandler.mainView.ViewSettingsPanel();
				}else{
					alert = Alert.show( "Incorrect password, please try again!", "Warning", new ArrayCollection(
						[
							{ label: "OK"},
							{ label: "CANCEL", triggered: cancelButton_triggeredHandler }
						]) );
				}
			}
		}

		private function cancelButton_triggeredHandler():void
		{
			this.removeFromParent();
			passInput.text = "";
		}
		
		override protected function draw():void
		{	
			layoutVerTopCenter.gap = 5;
			
			mainCont.setSize(actualWidth,actualHeight);			
			label.setSize(actualWidth * 0.2, 30);
			passInput.setSize(actualWidth * 0.2, 40);
			passInputConfirm.setSize(actualWidth * 0.2, 40);
			passBtn.setSize(actualWidth * 0.2, 40);
			
			//			buttonsGroup.paddingTop = 30;
		}
	}
}