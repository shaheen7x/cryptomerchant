package screens
{
	import flash.display.StageDisplayState;
	
	import mx.core.mx_internal;
	
	import assets.Assets;
	
	import data.DataHandler;
	
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.LayoutGroup;
	import feathers.core.FeathersControl;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalAlign;
	import feathers.layout.VerticalLayout;
	import feathers.skins.ImageSkin;
	
	import objects.BalancesBoard;
	import objects.QRCodeHolder;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.events.Event;
	
	public class MainView extends FeathersControl
	{
		private var layoutVerTopCenter:VerticalLayout;
		private var layoutVerMedCenter:VerticalLayout;
		private var layoutHorMedCenter:HorizontalLayout;
		private var mainCont:LayoutGroup;
		private var headerCont:LayoutGroup;
		private var dataCont:LayoutGroup;
		
		private var header:Header;
		
		private var settingsBtn:Button;
		
		public var qrHolder:QRCodeHolder;
		
		public var customerPanel:CustomerPanel;
		private var layoutHorTopCenter:HorizontalLayout;
		
		private var settingsPanel:SettingsPanel;
		private var scanScreen:ScanScreen;
//		public var balanceBoard:BalancesBoard;
		private var passwordpanel:PasswordPanel;

		
		public function MainView()
		{
//			super();
		}
		override protected function initialize():void
		{
			DataHandler.mainView = this;
			
			layoutVerTopCenter = new VerticalLayout;
			layoutVerTopCenter.verticalAlign =  VerticalAlign.TOP;
			layoutVerTopCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutVerMedCenter = new VerticalLayout;
			layoutVerMedCenter.verticalAlign =  VerticalAlign.MIDDLE;
			layoutVerMedCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutHorMedCenter = new HorizontalLayout;
			layoutHorMedCenter.verticalAlign =  VerticalAlign.MIDDLE;
			layoutHorMedCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutHorTopCenter = new HorizontalLayout;
			layoutHorTopCenter.verticalAlign =  VerticalAlign.TOP;
			layoutHorTopCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			mainCont = new LayoutGroup;
			mainCont.layout = layoutVerTopCenter;			
//			mainCont.backgroundSkin = new Quad(2,2,0x);
			
			headerCont = new LayoutGroup;
			headerCont.layout = layoutHorMedCenter;
//			headerCont.backgroundSkin = new Quad(2,2,0x171717);
			dataCont = new LayoutGroup;
			dataCont.layout = layoutHorTopCenter;
//			headerCont.backgroundSkin = new Quad(2,2,0x171717);
			header = new Header;
			header.title = "Cryptos Merchant - Locked Version";
			
//			balanceBoard = new BalancesBoard;
			
//			header.leftItems = new Vector.<DisplayObject>;
//			header.leftItems.push(balanceBoard);
			

			
			settingsBtn = new Button;
			settingsBtn.defaultIcon = new Image(Assets.assetsManager.getTexture("settings"));
			settingsBtn.addEventListener(Event.TRIGGERED, onSettingsBtnTriggered);
			
			header.rightItems = new Vector.<DisplayObject>;
//			header.rightItems.push(coinbaseBtn);
			header.rightItems.push(settingsBtn);

			passwordpanel = new PasswordPanel;
			
			customerPanel = new CustomerPanel;
			qrHolder = new QRCodeHolder;
			
			
			

			
			dataCont.addChild(customerPanel);
			dataCont.addChild(qrHolder);
			
			mainCont.addChild(header);
			mainCont.addChild(headerCont);
			mainCont.addChild(dataCont);

			
			this.addChild(mainCont);
		}
		

		private function onSettingsBtnTriggered():void
		{
			this.addChild(passwordpanel);
			
//			ViewSettingsPanel();

		}
		public function ViewSettingsPanel():void
		{
			settingsPanel = new SettingsPanel;
			this.addChild(settingsPanel);
			
			settingsPanel.validate();
			
//			trace(actualHeight);
			settingsPanel.setSize(actualWidth,actualHeight);
		}
		
		public function ViewScanScreen():void
		{
			scanScreen = new ScanScreen;
			this.parent.addChild(scanScreen);			
			scanScreen.validate();
			Starling.current.viewPort.width = Starling.current.nativeStage.fullScreenWidth;
			Starling.current.viewPort.height = Starling.current.nativeStage.fullScreenHeight;
			scanScreen.setSize(actualWidth,actualHeight);
			Starling.current.nativeStage.stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;			
		}
		
		override protected function draw():void
		{
			layoutVerTopCenter.gap = 4;
			layoutVerMedCenter.gap = 4;
			layoutHorMedCenter.gap = 4;
			mainCont.setSize(actualWidth,actualHeight);
			header.setSize(actualWidth, actualHeight * 0.1);
			headerCont.setSize(actualWidth,actualHeight * 0.05);
			dataCont.setSize(actualWidth,actualHeight * 0.95);	
			
//			balanceBoard.setSize(actualWidth * 0.78, 40);
			
			settingsBtn.setSize(40,40);


			customerPanel.setSize(actualWidth * 0.55, dataCont.height);
			qrHolder.setSize(actualWidth * 0.44, dataCont.height * 0.87);
			
			passwordpanel.setSize(actualWidth , actualHeight);
			
//			titleLabel.setSize(240, 40);			
//			playList.setSize(240,actualHeight - 45);
		}
	}
}