package screens
{
	import flash.display.Bitmap;
	import flash.display.StageDisplayState;
	import flash.events.KeyboardEvent;
	
	import assets.Assets;
	
	import data.DataHandler;
	
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.ImageLoader;
	import feathers.controls.LayoutGroup;
	import feathers.controls.ScrollContainer;
	import feathers.core.FeathersControl;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalAlign;
	import feathers.layout.VerticalLayout;
	
	import objects.InvoiceLine;
	
	import org.qrcode.QRCode;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.textures.Texture;
	
	public class ScanScreen extends FeathersControl
	{
		private var layoutVerTopCenter:VerticalLayout;
		private var layoutVerMedCenter:VerticalLayout;
		private var layoutHorMedCenter:HorizontalLayout;
		private var layoutHorTopCenter:HorizontalLayout;
		private var mainCont:LayoutGroup;
		private var bodyCont:LayoutGroup;
		private var invoiceCont:ScrollContainer;
		private var qr:QRCode;
		private var qrCodeImage:ImageLoader;
		private var header:Header;
		private var closeBtn:Button;
		private var invoiceHeader:InvoiceLine;
		private var headerSeperator:Quad;
		private var totalSeperator:Quad;
		private var invoiceTotal:InvoiceLine;
		private var invoiceLine:InvoiceLine;
		private var invoiceLinesArray:Array;
		
		public function ScanScreen()
		{
			super();
		}
		override protected function initialize():void
		{			
			layoutVerTopCenter = new VerticalLayout;
			layoutVerTopCenter.verticalAlign =  VerticalAlign.TOP;
			layoutVerTopCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutVerMedCenter = new VerticalLayout;
			layoutVerMedCenter.verticalAlign =  VerticalAlign.MIDDLE;
			layoutVerMedCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutHorMedCenter = new HorizontalLayout;
			layoutHorMedCenter.verticalAlign =  VerticalAlign.MIDDLE;
			layoutHorMedCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutHorTopCenter = new HorizontalLayout;
			layoutHorTopCenter.verticalAlign =  VerticalAlign.TOP;
			layoutHorTopCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			mainCont = new LayoutGroup;
			mainCont.layout = layoutVerTopCenter;
			mainCont.backgroundSkin = new Quad(2,2,0x333333);
			
			header = new Header;
			header.title = "Settings";			
			closeBtn = new Button;
			closeBtn.defaultIcon = new Image(Assets.assetsManager.getTexture("close"));
			closeBtn.addEventListener(Event.TRIGGERED, onBtnCloseTriggered);
			header.leftItems = new Vector.<DisplayObject>;
			header.leftItems.push(closeBtn);
			
			bodyCont = new LayoutGroup;
			bodyCont.layout = layoutHorMedCenter;	
			
			invoiceCont = new ScrollContainer;
			invoiceCont.layout = layoutVerTopCenter;
			invoiceHeader = new InvoiceLine("Item", "Price", "Quantity",0,"Amount");
			headerSeperator = new Quad(2,2,0xfcfcfc);
			invoiceCont.addChild(invoiceHeader);
			invoiceCont.addChild(headerSeperator);
			invoiceLinesArray = new Array;
			for(var i:int=0; i<DataHandler.tempInvoiceArray.length; i++)
			{
				invoiceLine = new InvoiceLine("item", "0", "0");
				invoiceCont.addChild(invoiceLine);
				invoiceLinesArray.push(invoiceLine);
			}
			totalSeperator = new Quad(2,2,0xfcfcfc);			
			invoiceTotal = new InvoiceLine("TOTAL", "", "");
			invoiceCont.addChild(totalSeperator);
			invoiceCont.addChild(invoiceTotal);

			
			qr = new QRCode;
			qrCodeImage = new ImageLoader;
			CreatQRCodeImage(DataHandler.selectedWalletAddress);
			

			bodyCont.addChild(invoiceCont);
			bodyCont.addChild(qrCodeImage);
			
			mainCont.addChild(header);
			mainCont.addChild(bodyCont);
			
			this.addChild(mainCont);
			
			Starling.current.nativeStage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
		}
		private function updateInvoiceData():void
		{
			if(invoiceLinesArray.length > 0)
			{
				var item:String;
				var price:String;
				var quantity:String;
				var total:Number = 0;
				trace("DataHandler.tempInvoiceArray.length "+DataHandler.tempInvoiceArray.length);
				for(var i:int=0; i<invoiceLinesArray.length; i++)
				{
					item = DataHandler.tempInvoiceArray[i].item;
					price = DataHandler.tempInvoiceArray[i].price
					quantity = DataHandler.tempInvoiceArray[i].quantity;
					total += Number(DataHandler.tempInvoiceArray[i].amount);
					trace(item+" "+price+" "+quantity);
					invoiceLinesArray[i].UpdateVlaues(item, price, quantity);
					invoiceLinesArray[i].setSize(actualWidth*0.45,15);
//					invoiceLine.invalidate();
					trace("total "+total);
					
				}
				trace("total.toFixed(2) "+total.toFixed(2));
				invoiceTotal.UpdateVlaues("TOTAL", "", "",0,total.toFixed(2));
//				invoiceTotal.validate();
			}
			
		}
		
		protected function onKeyDown(event:KeyboardEvent):void
		{
			if(event.charCode == 27)
			{
				onBtnCloseTriggered();
			}
		}
		
		private function CreatQRCodeImage(str:String):void
		{
			var bitmap:Bitmap;
			qr.encode(str);
			bitmap = new Bitmap(qr.bitmapData);
			bitmap.smoothing = true;
			var texture:Texture = Texture.fromBitmap(bitmap);
			qrCodeImage.source = texture;
		}
		
		private function onBtnCloseTriggered():void
		{
			this.removeFromParent(true);
			Starling.current.viewPort.width = 950;
			Starling.current.viewPort.height = 750;
			Starling.current.nativeStage.stage.displayState = StageDisplayState.NORMAL;
			Starling.current.nativeStage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
		}
		
		override protected function draw():void
		{
			layoutVerTopCenter.gap = 4;
			layoutVerMedCenter.gap = 4;
			layoutHorMedCenter.gap = actualWidth * 0.06;
			mainCont.setSize(actualWidth,actualHeight);
			header.setSize(actualWidth, actualHeight * 0.1);
			closeBtn.setSize(40,40);
			bodyCont.setSize(actualWidth,actualHeight * 0.89);
			qrCodeImage.setSize(actualWidth * 0.4, actualWidth * 0.4);
			invoiceCont.setSize(actualWidth * 0.5,bodyCont.height);
			invoiceHeader.setSize(actualWidth *0.45,30);
			invoiceTotal.setSize(actualWidth *0.45,30);
			headerSeperator.width = totalSeperator.width = actualWidth * 0.5;
			
			updateInvoiceData();

		}
	}
}