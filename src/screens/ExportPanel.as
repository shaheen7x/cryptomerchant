package screens
{
	import data.DataHandler;
	
	import feathers.controls.ButtonGroup;
	import feathers.controls.Header;
	import feathers.controls.LayoutGroup;
	import feathers.core.FeathersControl;
	import feathers.data.ArrayCollection;
	import feathers.layout.Direction;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.VerticalAlign;
	import feathers.layout.VerticalLayout;
	
	import starling.events.Event;
	
	public class ExportPanel extends FeathersControl
	{
		private var layoutVerTopCenter:VerticalLayout;
		private var mainCont:LayoutGroup;
		private var header:Header;
		private var buttonGroup:ButtonGroup;
		public function ExportPanel()
		{
			super();
		}
		override protected function initialize():void
		{
			layoutVerTopCenter = new VerticalLayout;
			layoutVerTopCenter.verticalAlign =  VerticalAlign.TOP;
			layoutVerTopCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			mainCont = new LayoutGroup;
			mainCont.layout = layoutVerTopCenter;
			
			
			header = new Header;
			header.title = "Data Settings";
			
			buttonGroup = new ButtonGroup;
			buttonGroup.direction = Direction.HORIZONTAL;
			buttonGroup.dataProvider = new ArrayCollection(
				[
					{ label: "Export Data to CSV", triggered: onSaveBtnTriggered},
					//					{ label: "Scan QR", triggered: qrBtn_triggeredHandler },
					{ label: "Clear Data", triggered: onClearBtnTriggered },
				]);
			
			mainCont.addChild(header);
			mainCont.addChild(buttonGroup);
			this.addChild(mainCont);
		}
		private function onSaveBtnTriggered(e:Event):void
		{
			DataHandler.ExportData();
		}
		private function onClearBtnTriggered(e:Event):void
		{
			DataHandler.ClearData();
		}
		override protected function draw():void
		{	
			layoutVerTopCenter.gap = 20;
			
			mainCont.setSize(actualWidth,actualHeight);			
			header.setSize(actualWidth, actualHeight * 0.2);
			buttonGroup.setSize(actualWidth * 0.4, 40);
			buttonGroup.gap = 30;
//			buttonsGroup.paddingTop = 30;
		}
	}
}