package screens
{
	import assets.Assets;
	
	import data.DataHandler;
	
	import feathers.controls.Alert;
	import feathers.controls.AutoComplete;
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.TextInput;
	import feathers.controls.ToggleButton;
	import feathers.controls.ToggleSwitch;
	import feathers.core.FeathersControl;
	import feathers.core.ToggleGroup;
	import feathers.data.ArrayCollection;
	import feathers.data.LocalAutoCompleteSource;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.TiledRowsLayout;
	import feathers.layout.VerticalAlign;
	import feathers.layout.VerticalLayout;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.text.TrueTypeCompositor;
	
	public class CustomerPanel extends FeathersControl
	{
		private var layoutVerTopCenter:VerticalLayout;
		private var layoutVerMedCenter:VerticalLayout;
		private var layoutHorMedCenter:HorizontalLayout;
		private var layoutTiledRows:TiledRowsLayout;
		private var mainCont:LayoutGroup;
		private var headerCont:LayoutGroup;
		private var dataCont:LayoutGroup;
		
		private var header:Header;
		
		private var idLabel:Label;
		private var idInput:TextInput;
		private var nameLabel:Label;
		private var nameInput:TextInput;
		private var addressLabel:Label;
		private var addressInput:TextInput;
		private var phoneLabel:Label;
		private var phoneInput:TextInput;
		private var emailLabel:Label;
		private var emailInput:TextInput;
		private var saveBtn:Button;
		private var customersAutoComplete:AutoComplete;
		private var newBtn:ToggleButton;
		private var toggleGroup:ToggleGroup;

		
		public var invoicePanel:InvoicePanel;
		private var oldBtn:ToggleButton;
		private var isNew:Boolean = true;

		
		public function CustomerPanel()
		{
			super();
		}
		override protected function initialize():void
		{
			layoutVerTopCenter = new VerticalLayout;
			layoutVerTopCenter.verticalAlign =  VerticalAlign.TOP;
			layoutVerTopCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutVerMedCenter = new VerticalLayout;
			layoutVerMedCenter.verticalAlign =  VerticalAlign.MIDDLE;
			layoutVerMedCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutHorMedCenter = new HorizontalLayout;
			layoutHorMedCenter.verticalAlign =  VerticalAlign.MIDDLE;
			layoutHorMedCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutTiledRows = new TiledRowsLayout;
			layoutTiledRows.verticalAlign = VerticalAlign.MIDDLE;
			layoutTiledRows.horizontalAlign = HorizontalAlign.CENTER;
			layoutTiledRows.tileHorizontalAlign = HorizontalAlign.LEFT;
			layoutTiledRows.tileVerticalAlign = VerticalAlign.MIDDLE;
			layoutTiledRows.useSquareTiles = false;
			layoutTiledRows.requestedColumnCount = 2;			
			
			mainCont = new LayoutGroup;
			mainCont.layout = layoutVerTopCenter;			
//			mainCont.backgroundSkin = new Quad(2,2,0x515122);
			
			header = new Header;
			header.title = "Customer Data";
			
			headerCont = new LayoutGroup;
			headerCont.layout = layoutHorMedCenter;
//			headerCont.backgroundSkin = new Quad(2,2,0xc5d6a2);
			toggleGroup = new ToggleGroup;
			newBtn = new ToggleButton;
			newBtn.label = "New";
			newBtn.toggleGroup = toggleGroup;
			
			oldBtn = new ToggleButton;
			oldBtn.label = "Existed";
			oldBtn.toggleGroup = toggleGroup;
			
			toggleGroup.addEventListener(Event.CHANGE, onToggleChanged);
			
			customersAutoComplete = new AutoComplete;
			customersAutoComplete.prompt = "Please type here to search in customers";
			customersAutoComplete.source = new LocalAutoCompleteSource(new ArrayCollection(DataHandler.customersDataArray));
			customersAutoComplete.addEventListener(Event.CLOSE, onAutoCompleteClosed);
			customersAutoComplete.minimumAutoCompleteLength = 1;
			customersAutoComplete.isEnabled = false;
			saveBtn = new Button;
			saveBtn.defaultIcon = new Image(Assets.assetsManager.getTexture("save"));
			saveBtn.addEventListener(Event.TRIGGERED, onSaveBtnTriggered);
			
			headerCont.addChild(newBtn);
			headerCont.addChild(oldBtn);
			headerCont.addChild(customersAutoComplete);
//			headerCont.addChild(saveBtn);
			
			dataCont = new LayoutGroup;
			dataCont.layout = layoutTiledRows;
//			dataCont.backgroundSkin = new Quad(2,2,0xc5d6a2);
			
			idLabel = new Label;
			idLabel.text = "Customer ID:";
			idInput = new TextInput;
			idInput.isEditable = false;
			idInput.isEnabled = false;
			
			nameLabel = new Label;
			nameLabel.text = "Name:";
			nameInput = new TextInput;
			
			addressLabel = new Label;
			addressLabel.text = "Adress:";
			addressInput = new TextInput;
			
			phoneLabel = new Label;
			phoneLabel.text = "Phone:";
			phoneInput = new TextInput;
			
			emailLabel = new Label;
			emailLabel.text = "email:";
			emailInput = new TextInput;
			
			dataCont.addChild(idLabel);
			dataCont.addChild(idInput);
			dataCont.addChild(nameLabel);
			dataCont.addChild(nameInput);
			dataCont.addChild(addressLabel);
			dataCont.addChild(addressInput);
			dataCont.addChild(phoneLabel);
			dataCont.addChild(phoneInput);
			dataCont.addChild(emailLabel);
			dataCont.addChild(emailInput);
			
			invoicePanel = new InvoicePanel;
			
			mainCont.addChild(header);
			mainCont.addChild(headerCont);
			mainCont.addChild(dataCont);
			mainCont.addChild(invoicePanel);
			this.addChild(mainCont);
						
		}
		
		private function onToggleChanged(e:Event):void
		{
			switch(toggleGroup.selectedIndex)
			{
				case 0:
					customersAutoComplete.isEnabled = false;
					nameInput.isEnabled = true;
					addressInput.isEnabled = true;
					phoneInput.isEnabled = true;
					emailInput.isEnabled = true;
					saveBtn.isEnabled = true;
					isNew = true;
					customersAutoComplete.text = "";
					nameInput.text="";
					addressInput.text="";
					phoneInput.text="";
					emailInput.text="";
					
					break;
				case 1:
					customersAutoComplete.isEnabled = true;
					nameInput.isEnabled = false;
					addressInput.isEnabled = false;
					phoneInput.isEnabled = false;
					emailInput.isEnabled = false;
					saveBtn.isEnabled = false;
					isNew = false;
					break;
			}
			
		}
		
		private function onAutoCompleteClosed():void
		{
			var id:int = DataHandler.customersDataArray.indexOf(customersAutoComplete.text);
			trace("id "+id);
			if(id!=-1)
			{
				//			trace(customersAutoComplete.text + " "+ id);
				//			trace(DataHandler.customersArray[id].phone);
				idInput.text=DataHandler.customersArray[id].id;
				nameInput.text=DataHandler.customersArray[id].name;
				addressInput.text=DataHandler.customersArray[id].address;
				phoneInput.text=DataHandler.customersArray[id].phone;
				emailInput.text=DataHandler.customersArray[id].email;
				DataHandler.selectedCustomerID = id;
			}
		}
		
		private function onSaveBtnTriggered():void
		{
			SaveCustomerData();
		}
		public function SaveCustomerData():void
		{
			if(isNew)
			{
				if(nameInput.text!="" && addressInput.text!="" && phoneInput.text!="" && emailInput.text!="")
				{
					DataHandler.AddNewCustomer(nameInput.text,addressInput.text,phoneInput.text,emailInput.text);
					toggleGroup.selectedIndex = 1;
					customersAutoComplete.text = nameInput.text;
					//				nameInput.text="";
					//				addressInput.text="";
					//				phoneInput.text="";
					//				emailInput.text="";
					

				}else{
					var alert:Alert = Alert.show( "Please complete customer's missing data.", "Warning", new ArrayCollection(
						[
							{ label: "OK"}
						]) );
				}
			}

		}
		
		public function Clear():void
		{
			this.removeChildren(0,-1,true);
			this.initialize();
			this.draw();
//			DataHandler.selectedCustomerID = -1;
//			toggleGroup.selectedIndex = 0;
//			customersAutoComplete.isEnabled = false;
//			nameInput.isEnabled = true;
//			addressInput.isEnabled = true;
//			phoneInput.isEnabled = true;
//			emailInput.isEnabled = true;
//			saveBtn.isEnabled = true;
//			
//			customersAutoComplete.text = "";
//			nameInput.text="";
//			addressInput.text="";
//			phoneInput.text="";
//			emailInput.text="";
		}
		override protected function draw():void
		{
			layoutVerTopCenter.gap = 4;
			layoutVerMedCenter.gap = 4;
			layoutHorMedCenter.gap = 4;
			layoutTiledRows.gap = 4;
			
			mainCont.setSize(actualWidth,actualHeight);		
			header.setSize(actualWidth, actualHeight * 0.05);
			headerCont.setSize(actualWidth,actualHeight * 0.08);
			newBtn.setSize(65,35);
			oldBtn.setSize(65,35);
			customersAutoComplete.setSize(actualWidth * 0.65,35);
			saveBtn.setSize(35,35);
			dataCont.setSize(actualWidth,actualHeight * 0.27);	
			

			
			invoicePanel.setSize(actualWidth, actualHeight * 0.47);
			
			idLabel.setSize(100, 35);
			idInput.setSize(200, 35);
			nameLabel.setSize(100, 35);
			nameInput.setSize(200, 35);
			addressLabel.setSize(100, 35);
			addressInput.setSize(200, 35);
			phoneLabel.setSize(100, 35);
			phoneInput.setSize(200, 35);
			emailLabel.setSize(100, 35);
			emailInput.setSize(200, 35);
			//			titleLabel.setSize(230, 30);			
			//			playList.setSize(230,actualHeight - 45);
		}
	}
}