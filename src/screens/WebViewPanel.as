package screens
{
	import flash.geom.Rectangle;
	import flash.media.StageWebView;
	
	import assets.Assets;
	
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.LayoutGroup;
	import feathers.controls.WebView;
	import feathers.core.FeathersControl;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.VerticalAlign;
	import feathers.layout.VerticalLayout;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	
	public class WebViewPanel extends FeathersControl
	{
		private var layoutVerTopCenter:VerticalLayout;
		private var mainCont:LayoutGroup;
		private var header:Header;
		private var closeBtn:Button;
		public var stageWebView:StageWebView;
		private var _url:String;
		private var _title:String;
		public function WebViewPanel(title:String = "", url:String = "http://www.google.com")
		{
			_url = url;
			_title = title
			super();
		}
		override protected function initialize():void
		{
			layoutVerTopCenter = new VerticalLayout;
			layoutVerTopCenter.verticalAlign =  VerticalAlign.TOP;
			layoutVerTopCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			mainCont = new LayoutGroup;
			mainCont.layout = layoutVerTopCenter;
			mainCont.backgroundSkin = new Quad(2,2,0x333333);
			
			header = new Header;
			header.title = _title;			
			closeBtn = new Button;
			closeBtn.defaultIcon = new Image(Assets.assetsManager.getTexture("close"));
			closeBtn.addEventListener(Event.TRIGGERED, onBtnCloseTriggered);
			header.rightItems = new Vector.<DisplayObject>;
			header.rightItems.push(closeBtn);
			header.validate();
			
			stageWebView= new StageWebView();
			stageWebView.stage = Starling.current.nativeStage;
			stageWebView.viewPort = new Rectangle(0,75,stage.stageWidth,stage.stageHeight-75);
			
			mainCont.addChild(header);
//			mainCont.addChild(webView);
			this.addChild(mainCont);
			stageWebView.loadURL(_url);
		}
		override protected function draw():void
		{	
			layoutVerTopCenter.gap = 10;
			mainCont.setSize(actualWidth,actualHeight);
			
			header.setSize(actualWidth, 75);
			closeBtn.setSize(40,40);
//			webView.setSize(actualWidth * 0.95, actualHeight * 0.85);
		}
		
		private function onBtnCloseTriggered():void
		{
			this.removeFromParent(true);
			stageWebView.dispose();
		}
	}
}