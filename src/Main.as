package
{
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.geom.Rectangle;
	
	import core.MainApp;
	
	import data.DataHandler;
	
	import starling.core.Starling;
	
	[SWF(width="950", height="750", frameRate="60", backgroundColor="#ffffff")]
	
	public class Main extends Sprite
	{
		private var _starling:Starling;
		
		public function Main()
		{
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.nativeWindow.x = (stage.fullScreenWidth - 950)/2;
			stage.nativeWindow.y = (stage.fullScreenHeight - 750)/2;
			trace(stage.fullScreenWidth+" "+stage.fullScreenHeight);
			//			trace("hello\tworld\ltest"); 
			DataHandler.getAppData();
			stage.nativeWindow.addEventListener(Event.CLOSE, onExit);
			//			stage.nativeWindow.addEventListener(Event.CLOSE, onExit);
			var viewPort:Rectangle = new Rectangle(0,0,stage.stageWidth,stage.stageHeight);
			_starling = new Starling(MainApp, stage, viewPort);
			_starling.start();
		}
		protected function onExit(event:Event):void
		{
			DataHandler.StoreAppData();
		}
	}
}